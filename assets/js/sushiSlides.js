(function(document, window){
    let _slides = [],
        _footer = {},
        _progressbar = {},
        _currentSlideIndex = 0,
        _timer = null;


    function SushiSlides(){
        var me = this;
        me.init();
    };

    SushiSlides.prototype = {
        handlers: [
            {
                name: 'showFooter',
                isDefaultHandler: true,
                handler: function(){
                    _footer.classList.remove('hidden');
                }
            },
            {
                name: 'hideFooter',
                handler: function(){
                    _footer.classList.add('hidden');
                }
            }
        ],
        init(){
            _addEventListeners(this);
            _loadProgressbar(this);
            _loadSlides(this);
            _loadFooter(this);
            _fetchSlideFromUrl(this);
            _showCurrentSlide(this);
        },

        addHandler(name, func){
            this.handlers.push({
                name: name,
                handler: func
            });
        },
        
        runHandlers() {
            let currentSlide = _slides[_currentSlideIndex],
                me = this;
    
            me.handlers.forEach(element => {
                if (element.isDefaultHandler) {
                    element.handler();
                    return;
                }

                if (currentSlide.dataset.hasOwnProperty(element.name)) {
                    element.handler();
                }
            });
        }
    };

    function _loadSlides() {
        _slides = document.querySelectorAll(".slides section");
    }

    function _loadFooter() {
        _footer = document.querySelector("footer");
    }

    function _showCurrentSlide(sushi) {
        clearTimeout(_timer);
        _timer = setTimeout(function(){
            let currentSlide = _slides[_currentSlideIndex];
            _toggleFooter();
            sushi.runHandlers();
            currentSlide.scrollIntoView({
                'behavior': 'smooth',

            });
            _updateProgress();
        }, 200);
    }

    function _toggleFooter(){
        let currentSlide = _slides[_currentSlideIndex];
        let hiddenClass = "hidden";

        if (currentSlide.dataset.hideFooter !== undefined) {
            _footer.classList.add(hiddenClass);
            return;
        }
        
    }

    function _addEventListeners(sushi) {
        /* KEYS */
        document.addEventListener('keydown', function(e){
            switch(e.key){
                /** Navigation **/
                case 'ArrowLeft':
                case 'ArrowRight':
                case 'Home':
                case 'End':
                    e.preventDefault();
                    e.stopPropagation();
                    _navigate(sushi, e.key);
                    break;
                case 'p':
                    e.preventDefault();
                    e.stopPropagation();
                    _changeMode(sushi, 'presentation');
                    break;
                case 's':
                    e.preventDefault();
                    e.stopPropagation();
                    _changeMode(sushi, 'speaker');
                    break;
                    
            }
        });
    }

    function _navigate(sushi, direction){
        switch (direction) {
            case "ArrowLeft":
                _currentSlideIndex--;
                break;
            case "ArrowRight":
                _currentSlideIndex++;
                break;
            case "Home":
                _currentSlideIndex = 0;
                break;
            case "End":
                _currentSlideIndex = _slides.length - 1;
                break;
        }

        /* Limit the index to an allowed range */
        _currentSlideIndex = Math.min(_slides.length - 1, Math.max(0, _currentSlideIndex));
        _addHistory(sushi);
        _showCurrentSlide(sushi);
    }

    function _addHistory()
    {
        history.pushState(null, null, '#' + _currentSlideIndex);
    }

    function _fetchSlideFromUrl()
    {
        hash = window.location.hash.substr(1);
        if (!hash) return;

        _currentSlideIndex = Number.parseInt(hash);
    }

    function _loadProgressbar()
    {
        _progressbar = document.querySelector('.progressbar');
    }

    function _updateProgress()
    {
        percentageComplete = 100 / (_slides.length - 1) * (_currentSlideIndex);
        _progressbar.querySelector('.progress').style.width = '' + percentageComplete + 'vw';
    }

    function _changeMode(mode){
        console.log('MODE', mode);
    }

    window.SushiSlides = SushiSlides;
})(document, window);
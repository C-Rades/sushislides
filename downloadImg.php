<?php
$f = fopen('example.html', 'r');
$urls = [];
while(($line = fgets($f)) !== false)
{
    $matches1 = [];
    preg_match('/(http.*)\)/', $line, $matches1);
    $matches2 = [];
    preg_match('/(http.*)"/', $line, $matches2);
    if(count($matches1) > 1) 
    {
        $urls[] = $matches1[1];
    }
    if(count($matches2) > 1) 
    {
        $urls[] = $matches2[1];
    }
}
fclose($f);
$urls = array_filter($urls, function($u) { return strpos($u, 'localhost') === false;});
$writtenFiles = [];
foreach($urls as $i => $url)
{
    $url = rtrim($url, ')');
    $fname = 'img' . $i . '.jpg';
    $out = fopen('assets/img/'.$fname,'w+');
    $in = fopen($url, 'r');
    stream_copy_to_stream($in, $out);
    $writtenFiles[$url] = $fname;
    fclose($in);
    fclose($out);
}

$log = '';

foreach($writtenFiles as $url => $file)
{
    $log = $log . "${url} ${file}\n";
}
file_put_contents('down.log', $log);
print_r($writtenFiles);
